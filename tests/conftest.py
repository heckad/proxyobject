from pytest import fixture


@fixture(scope="session")
def function_which_always_return_new_object():
    class Wrapped:
        def same_func(self):
            return 'I am same func'

    return lambda: Wrapped()


@fixture(scope="session")
def function_which_return_the_same_object():
    class Wrapped:
        def same_func(self):
            return 'I am same func'

    wrapped = Wrapped()
    return lambda: wrapped
