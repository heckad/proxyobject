from proxyobject import ProxyObject


def test_creating_and_proxying_attributes(function_which_always_return_new_object):
    proxy_to_object = ProxyObject(function_which_always_return_new_object)
    assert proxy_to_object.same_func() == 'I am same func'
