from setuptools import find_packages
from setuptools import setup

setup(
    name='proxyobject',
    version='1.0',
    description='Similar proxy object',
    author='Heckad',
    author_email='Heckad@yandex.ru',
    packages=find_packages(exclude=['tests', 'tests.*']),
    test_suite='tests'
)
