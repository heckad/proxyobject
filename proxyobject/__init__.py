import copy

from typing import Callable, Any

__version_info__ = ('0', '1', '0')
__version__ = '.'.join(__version_info__)


class ProxyObject:
    """Forwards all operations toa proxied object.
    The only operations not supported for forwarding
    are right handed operands and any kind of assignment.

    Example usage::

        from proxyobject import ProxyObject
        proxy = ProxyObject(function_which_return_object)


    Whenever something is bound to l.user / l.request the proxy objects
    will forward all operations.  If no object is bound a :exc:`RuntimeError`
    will be raised.

    To create proxies to :class:`Local` or :class:`LocalStack` objects,
    call the object as shown above.  If you want to have a proxy to an
    object looked up by a function, you can (as of Werkzeug 0.6.1) pass
    a function to the :class:`LocalProxy` constructor::

        session = LocalProxy(lambda: get_current_request().session)

    .. versionchanged:: 0.6.1
       The class can be instantiated with a callable as well now.
    """
    __slots__ = ['__object_getter']

    def __init__(self, object_getter: Callable[[], object]):
        object.__setattr__(self, '_ProxyObject__object_getter', object_getter)

    def __get_wrapped(self) -> Any:
        return object.__getattribute__(self, '_ProxyObject__object_getter')()

    def __repr__(self): return '<ProxyObject to {}>'.format(repr(self.__get_wrapped()))

    def __setitem__(self, key, value):
        self.__get_wrapped()[key] = value

    def __delitem__(self, key):
        del self.__get_wrapped()[key]

    __name__ = property(lambda self: self.__get_wrapped().__name__)
    __dict__ = property(lambda self: self.__get_wrapped().__dict__)
    def __dir__(self): return dir(self.__get_wrapped())

    def __getattr__(x, n): return getattr(x.__get_wrapped(), n)
    def __setattr__(x, n, v): return setattr(x.__get_wrapped(), n, v)
    def __delattr__(x, n): return delattr(x.__get_wrapped(), n)

    def __int__(x): return int(x.__get_wrapped())
    def __bool__(self): return bool(self.__get_wrapped())
    def __float__(x): return float(x.__get_wrapped())
    def __complex__(x): return complex(x.__get_wrapped())
    def __str__(x): return str(x.__get_wrapped())

    def __lt__(x, o): return x.__get_wrapped() < o
    def __le__(x, o): return x.__get_wrapped() <= o
    def __eq__(x, o): return x.__get_wrapped() == o
    def __ne__(x, o): return x.__get_wrapped() != o
    def __gt__(x, o): return x.__get_wrapped() > o
    def __ge__(x, o): return x.__get_wrapped() >= o
    def __hash__(x): return hash(x.__get_wrapped())
    __call__ = lambda x, *a, **kw: x.__get_wrapped()(*a, **kw)
    def __len__(x): return len(x.__get_wrapped())
    def __getitem__(x, i): return x.__get_wrapped()[i]
    def __iter__(x): return iter(x.__get_wrapped())
    def __reversed__(x): return reversed(x.__get_wrapped())
    def __contains__(x, i): return i in x.__get_wrapped()
    def __add__(x, o): return x.__get_wrapped() + o
    def __sub__(x, o): return x.__get_wrapped() - o
    def __mul__(x, o): return x.__get_wrapped() * o
    def __floordiv__(x, o): return x.__get_wrapped() // o
    def __mod__(x, o): return x.__get_wrapped() % o
    def __divmod__(x, o): return x.__get_wrapped().__divmod__(o)
    def __pow__(x, o): return x.__get_wrapped() ** o
    def __lshift__(x, o): return x.__get_wrapped() << o
    def __rshift__(x, o): return x.__get_wrapped() >> o
    def __and__(x, o): return x.__get_wrapped() & o
    def __xor__(x, o): return x.__get_wrapped() ^ o
    def __or__(x, o): return x.__get_wrapped() | o
    def __div__(x, o): return x.__get_wrapped().__div__(o)
    def __truediv__(x, o): return x.__get_wrapped().__truediv__(o)
    def __neg__(x): return -(x.__get_wrapped())
    def __pos__(x): return +(x.__get_wrapped())
    def __abs__(x): return abs(x.__get_wrapped())
    def __invert__(x): return ~(x.__get_wrapped())
    def __oct__(x): return oct(x.__get_wrapped())
    def __hex__(x): return hex(x.__get_wrapped())
    def __index__(x): return x.__get_wrapped().__index__()
    def __coerce__(x, o): return x.__get_wrapped().__coerce__(x, o)
    def __enter__(x): return x.__get_wrapped().__enter__()
    __exit__ = lambda x, *a, **kw: x.__get_wrapped().__exit__(*a, **kw)
    def __radd__(x, o): return o + x.__get_wrapped()
    def __rsub__(x, o): return o - x.__get_wrapped()
    def __rmul__(x, o): return o * x.__get_wrapped()
    def __rdiv__(x, o): return o / x.__get_wrapped()
    def __rfloordiv__(x, o): return o // x.__get_wrapped()
    def __rmod__(x, o): return o % x.__get_wrapped()
    def __rdivmod__(x, o): return x.__get_wrapped().__rdivmod__(o)
    def __copy__(x): return copy.copy(x.__get_wrapped())
    def __deepcopy__(x, memo): return copy.deepcopy(x.__get_wrapped(), memo=memo)
